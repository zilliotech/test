<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Attendance extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
       $this->load->model('attendance_model');
    }

    public function classlist_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();

        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->attendance_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                $config = General::get_db_config($dataPost['college_code']);
                $conn = $this->attendance_model->conn_db($config);
                $cols = array('courses_offered.course_code', 'courses_offered.course_id', 'class_master.class_id', 'class_master.year_name', 'class_master.year', 'class_master.semester', 'class_master.class_division', 'subjects_offered.subject_id', 'subjects_offered.subject_name', 'subjects_offered.subject_code');
                $where = array('email' => $token->id);

                $classList = $this->attendance_model->get_class_list($cols, $where, $conn)->result();
                $response = [
                    'class_list' => $classList
                ];
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }


    public function studentlist_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();

        
        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->attendance_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        if(!isset($dataPost['class_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Class ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('class_id' => $dataPost['class_id']);
            $config = General::get_db_config($dataPost['college_code']);
            $conn = $this->attendance_model->conn_db($config);
            $result = $this->attendance_model->get_multiple_where('class_master', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Class ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        if(!isset($dataPost['subject_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Subject ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            $where = array('subject_id' => $dataPost['subject_id']);
            $config = General::get_db_config($dataPost['college_code']);
            $conn = $this->attendance_model->conn_db($config);
            $result = $this->attendance_model->get_multiple_where('subjects_offered', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Subject ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        };

       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                $config = General::get_db_config($dataPost['college_code']);
                $conn = $this->attendance_model->conn_db($config);
                $cols = array('student_master.class_id','student_master.student_id', 'student_master.roll_number', 'student_master.f_name', 'student_master.l_name', 'student_master.m_name', 'student_master.email', 'student_master.gender', 'student_master.dob', 'student_master.contact');
                $where = array('class_id' => $dataPost['class_id']);
                $studentList = $this->attendance_model->get_student_list($cols, $where, $conn)->result();
                $root_uri = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
                foreach ($studentList as $student) {
                    $cols = array('profile');
                    $where = array('email' => $student->email);
                    $profile_result = $this->attendance_model->get_cols_multiple_where($cols, 'login', $where, null)->row();
                    if(count($profile_result)>0){
                        $student->profile = $profile_result->profile;
                    }else{
                        $student->profile = '';
                    }
                    $where1 = array('class_master.class_id' => $dataPost['class_id'],'attendance_master.subject_id' => $dataPost['subject_id'],'attendance_list.student_id' => $student->student_id);
                    $cols1 = array('att_status', 'SUM(att_status * attendance_master.no_of_lecture) as lectures_attended', 'SUM(attendance_master.no_of_lecture) as total_lectures', 'ROUND(SUM(att_status * attendance_master.no_of_lecture)*100/SUM(attendance_master.no_of_lecture)) as perecentage_att');
                    $last_present = $this->attendance_model->get_student_last_att($cols1, $where1, $conn);

                   

                    if(count($last_present->row()) > 0){
                        $updated_last_present = $this->attendance_model->get_student_last_present(['att_status'], $where1, $conn)->row();
                      // print_r($last_present);
                        if(!isset($updated_last_present)){
                            $student->last_present = '0';
                        }else{
                            $student->last_present = $updated_last_present->att_status ? $updated_last_present->att_status : '0';
                        }
                         
                        $student->lectures_attended = $last_present->row()->lectures_attended ? $last_present->row()->lectures_attended : '0'; 
                        $student->total_lectures = $last_present->row()->total_lectures ? $last_present->row()->total_lectures : '0'; 
                        $student->perecentage_att = $last_present->row()->perecentage_att ? $last_present->row()->perecentage_att : '0'; 
                        
                    }else{
                        $student->last_present = '0';
                        $student->lectures_attended = '0';
                        $student->total_lectures = '0';
                        $student->perecentage_att = '0';
                    }

                    if(!$student->profile){
                       $student->profile = '';
                    }


                    if($student->profile!=''){
                       $student->profile = $root_uri."api/profile/".$student->profile;
                    } 
                }

                $response = [
                    'student_list' => $studentList
                ];
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function submit_attendance_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();

         if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->attendance_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        if(!isset($dataPost['class_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Class ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('class_id' => $dataPost['class_id']);
            $config = General::get_db_config($dataPost['college_code']);
            $conn = $this->attendance_model->conn_db($config);
            $result = $this->attendance_model->get_multiple_where('class_master', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Class ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        if(!isset($dataPost['subject_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Subject ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            $where = array('subject_id' => $dataPost['subject_id']);
            $config = General::get_db_config($dataPost['college_code']);
            $conn = $this->attendance_model->conn_db($config);
            $result = $this->attendance_model->get_multiple_where('subjects_offered', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Subject ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        };

       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                $config = General::get_db_config($dataPost['college_code']);
                $conn = $this->attendance_model->conn_db($config);

                $data = $dataPost;
                

                $where = array('class_id' => $dataPost['class_id'],'from_time' => $dataPost['from_time'],'to_time' => $dataPost['to_time'],'lecture_date' => $dataPost['lecture_date']);

                $checkData = $this->attendance_model->get_multiple_where('attendance_master',$where ,$conn)->result();


                if(count($checkData)>0){
                    $response = [
                        'status' => REST_Controller::HTTP_NOT_ACCEPTABLE,
                        'message' => 'Duplicate data entry',
                        'att_id' => $checkData[0]->id
                    ];
                    $this->set_response($response, REST_Controller::HTTP_NOT_ACCEPTABLE);
                    return;
                }

                unset($data['attendance_list'],$data['college_code']);
                $where = array('email' => $token->id);
                $professor_id = $this->attendance_model->get_cols_multiple_where(array('professor_id'), 'professor_master', $where, $conn);
                $data['professor_id'] = $professor_id->row()->professor_id;

                date_default_timezone_set('Asia/Kolkata'); 
                $now = date('Y-m-d H:i:s');
                
                $data['modified_on'] = $now;
                $data['created_on'] = $now;



                $id = $this->attendance_model->insert('attendance_master',$data,$conn);
                $cols = array('student_master.student_id');
                $where = array('class_id' => $dataPost['class_id']);
                $studentList = $this->attendance_model->get_student_list($cols, $where, $conn)->result();
                

                $attendance_list = $dataPost['attendance_list'];
                foreach ($studentList as $student) {
                    $student->att_id = $id;
                    $student->att_status = in_array($student->student_id ,explode(',',$attendance_list))?1:0;
                    $this->attendance_model->insert('attendance_list',$student,$conn);
                }

                $response = [
                    'status' => REST_Controller::HTTP_OK,
                    'message' => 'Attendance added successfully',
                    'att_id' => $id
                ];
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function student_class_attendance_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();
         if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->attendance_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        if(!isset($dataPost['year'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Year is Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }

        if(!isset($dataPost['month'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Month is Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        };

       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                $config = General::get_db_config($dataPost['college_code']);
                $conn = $this->attendance_model->conn_db($config);
                $cols = array('subjects_offered.subject_code','subjects_offered.subject_name', 'subjects_offered.subject_id', 'SUM(attendance_master.no_of_lecture) AS "total_lectures"', 'SUM(attendance_master.no_of_lecture * attendance_list.att_status) AS "attended_lectures"', 'ROUND(SUM(attendance_master.no_of_lecture * attendance_list.att_status) * 100 / SUM(attendance_master.no_of_lecture),0 ) AS "percent"');

                $where = array('email' => $token->id);
                $student_id = $this->attendance_model->get_cols_multiple_where(array('student_id'), 'student_master', $where, $conn);
                $class_id = $this->attendance_model->get_cols_multiple_where(array('class_id'), 'student_master', $where, $conn)->row()->class_id;

                $data['student_id'] = $student_id->row()->student_id;

                if($dataPost['month'] != 0){
                    $where = array('student_id' => $data['student_id'] ,'YEAR(attendance_master.lecture_date)' => $dataPost['year'],'MONTH(attendance_master.lecture_date)' => $dataPost['month']);
                }else{
                    $where = array('student_id' => $data['student_id'] ,'YEAR(attendance_master.lecture_date)' => $dataPost['year']);
                }

                
                $where_syllabus = array('class_id' => $class_id);

                $course_id = $this->attendance_model->get_cols_multiple_where(array('course_id'), 'class_master', $where_syllabus, $conn)->row()->course_id;

                $where_syllabus = array('course_id' => $course_id);
                
                $subject_list = $this->attendance_model->get_multiple_where('subjects_offered', $where_syllabus, $conn)->result();
                
                $attendancelist = $this->attendance_model->get_student_class_attendance($cols, $where, $conn)->result();

                $total_lectures = 0;
                $attended_lectures = 0;

                foreach ($subject_list as $subject) {
                   // print_r($subject);
                }

                foreach ($attendancelist as $att) {
                    $total_lectures = $total_lectures + $att->total_lectures;
                    $attended_lectures = $attended_lectures + $att->attended_lectures;
                }
               
                if($total_lectures!=0){
                    $overall_percentage = round($attended_lectures*100/$total_lectures);
                }else{
                    $overall_percentage = 0;
                }
              

                if($total_lectures == 0 && $attended_lectures == 0 && $overall_percentage ==0){
                    $attendancelist = array();
                }

                if($dataPost['month'] != 0){
                    $where_event = array('student_id' => $data['student_id'] ,'YEAR(event_master.event_date)' => $dataPost['year'],'MONTH(event_master.event_date)' => $dataPost['month']);
                }else{
                    $where_event = array('student_id' => $data['student_id'] ,'YEAR(event_master.event_date)' => $dataPost['year']);
                }
                

                $cols_event =  ['event_master.event_id, event_master.event_name, event_master.event_code, event_master.event_description, event_master.event_date, ROUND(SUM(event_attendance_list.attendance)) as event_attendance'];

                $result = $this->attendance_model->get_cols_events_group_join_where($cols_event, $where_event, $conn);
                
                $event_attendance_list = [];

                if(count($result->result()) > 0){

                    

                    $event_result = $result->result();
                    $event_attendance = 0;
                    foreach ($event_result as $event) {
                        $event_attendance += $event->event_attendance;
                        array_push($event_attendance_list, $event);
                    }

                    $attended_lectures = (string)((int)$attended_lectures + (int)$event_attendance); 
                    $total_lectures = (int)$total_lectures; 

                    if($total_lectures > 0){
                        $overall_percentage = ROUND(100 * (int)$attended_lectures / $total_lectures);
                        if($overall_percentage > 100){
                            $overall_percentage = "100";
                        }else{

                            $overall_percentage = (string)$overall_percentage;
                        }
                    }else{
                        $overall_percentage = "100";
                    }
                    
                }
                
                $response = [
                    'attendancelist' => $attendancelist,
                    'event_attendance_list'=> $event_attendance_list,
                    'total_lectures' => (string)$total_lectures,
                    'attended_lectures' => (string)$attended_lectures,
                    'overall_percentage' => (string)$overall_percentage
                ];


                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }


    public function college_class_attendance_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();

        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->attendance_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };
        
       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                $where = array('email' => $token->id,'login_detail_master.role_id'=>3, 'login_detail_master.college_id' => $this->getCollegeIDFromCode($dataPost['college_code']));
                $result = $this->attendance_model->get_join_multiple_where('*', $where, null)->row();
            
                if(count($result)<=0){
                    $response = [
                        'status' => REST_Controller::HTTP_UNAUTHORIZED,
                        'message' => 'Unauthorized',
                    ];
                    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                    return;
                }

                $config = General::get_db_config($dataPost['college_code']);
                $conn = $this->attendance_model->conn_db($config);
                $where = array();
                $cols = array('class_master.class_id','class_master.class_division','class_master.year','class_master.year_name','class_master.semester','courses_offered.course_name','courses_offered.course_code', 'CONCAT(class_master.year_name,courses_offered.course_code) as course_name');

                $class_list = $this->attendance_model->get_join_multiple_course_where($cols, $where, $conn)->result();

                foreach ($class_list as $class) {
                    $where = array('class_id' => $class->class_id);
                    $total_students = (string)count($this->attendance_model->get_student_count($where, $conn)->result()); 

                    $no_of_lecture = $this->attendance_model->get_cols_multiple_where(['COUNT(no_of_lecture) as no_of_lecture'],'attendance_master',['class_id'=>$class->class_id], $conn)->row()->no_of_lecture;


                    if(isset($no_of_lecture)){
                        $avg_present = $this->attendance_model->get_cols_multiple_join_where($class->class_id, $conn);
                        $avg_students = ROUND($avg_present/$no_of_lecture);
                    }else{
                        $avg_present = 'NA';
                        $avg_students = 'NA';
                    }

                    $class->total_students = (string)$total_students;
                    $class->avg_students = (string)$avg_students;
                }


                
                $response = (object) ['class_attendance_list' => $class_list];
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function student_attendance_list_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();

        
        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->attendance_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        if(!isset($dataPost['class_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Class ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('class_id' => $dataPost['class_id']);
            $config = General::get_db_config($dataPost['college_code']);
            $conn = $this->attendance_model->conn_db($config);
            $result = $this->attendance_model->get_multiple_where('class_master', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Class ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        

       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                $config = General::get_db_config($dataPost['college_code']);
                $conn = $this->attendance_model->conn_db($config);


                if(isset($dataPost['subject_id'])){
                    if($dataPost['subject_id'] != 0){
                        $where = array('subject_id' => $dataPost['subject_id']);
                        $result = $this->attendance_model->get_multiple_where('attendance_master', $where, $conn);
                        if(count($result->row()) <= 0){
                             /*$response = [
                            'status' => REST_Controller::HTTP_OK,
                            'message' => 'No records found for this Subject ID',
                            ];
                            $this->set_response($response, REST_Controller::HTTP_OK);
                            return;*/
                        }    
                    }
                }

                $from_date = '';
                $to_date = '';

                if(isset($dataPost['from_date'])){
                    if(!General::getDateValidation($dataPost['from_date'])){
                        $response = [
                        'status' => REST_Controller::HTTP_UNAUTHORIZED,
                        'message' => 'Invalid From Date',
                        ];
                        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                        return;
                    }
                    $from_date = date($dataPost['from_date']);
                }

                if(isset($dataPost['to_date'])){
                    if(!General::getDateValidation($dataPost['to_date'])){
                        $response = [
                        'status' => REST_Controller::HTTP_UNAUTHORIZED,
                        'message' => 'Invalid From Date',
                        ];
                        $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                        return;
                    }
                    $to_date = date($dataPost['to_date']);
                }


                if($from_date != '' && $to_date != ''){
                    $where = array('lecture_date >= ' => $from_date,'lecture_date <= ' => $to_date);
                    $result = $this->attendance_model->get_multiple_where('attendance_master', $where, $conn);
                    if(count($result->row()) <= 0){
                         $response = [
                        'status' => REST_Controller::HTTP_OK,
                        'message' => 'No records found for this Date Range',
                        ];
                        $this->set_response($response, REST_Controller::HTTP_OK);
                        return;
                    }  
                }





                $cols = array('student_master.class_id','student_master.student_id', 'student_master.roll_number', 'student_master.f_name', 'student_master.l_name', 'student_master.m_name', 'student_master.email', 'student_master.gender', 'student_master.dob', 'student_master.contact');
                $where = array('class_id' => $dataPost['class_id']);
                $studentList = $this->attendance_model->get_student_list($cols, $where, $conn)->result();
                $root_uri = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
                foreach ($studentList as $student) {
                    $cols = array('profile');
                    $where = array('email' => $student->email);
                    $profile_result = $this->attendance_model->get_cols_multiple_where($cols, 'login', $where, null)->row();
                    if(count($profile_result)>0){
                        $student->profile = $profile_result->profile;
                    }else{
                        $student->profile = '';
                    }



                    


                    if(!isset($dataPost['subject_id']) || $dataPost['subject_id'] == 0){
                        $where1 = array('class_master.class_id' => $dataPost['class_id'],'attendance_list.student_id' => $student->student_id);
                        if($from_date != '' && $to_date != ''){
                            $where1 = array('class_master.class_id' => $dataPost['class_id'],'attendance_list.student_id' => $student->student_id,'attendance_master.lecture_date >= ' => $from_date,'attendance_master.lecture_date <= ' => $to_date);
                        }
                    }else{
                        $where1 = array('class_master.class_id' => $dataPost['class_id'],'attendance_master.subject_id' => $dataPost['subject_id'],'attendance_list.student_id' => $student->student_id);
                        if($from_date != '' && $to_date != ''){
                            $where1 = array('class_master.class_id' => $dataPost['class_id'],'attendance_master.subject_id' => $dataPost['subject_id'],'attendance_list.student_id' => $student->student_id,'attendance_master.lecture_date >= ' => $from_date,'attendance_master.lecture_date <= ' => $to_date);
                        }
                        $where = array('subject_id' => $dataPost['subject_id']);
                        $result = $this->attendance_model->get_multiple_where('subjects_offered', $where, $conn);

                        if(count($result->row())<=0){
                            $response = [
                            'status' => REST_Controller::HTTP_UNAUTHORIZED,
                            'message' => 'Invalid Subject ID',
                            ];
                            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                            return;
                        }
                    };

                    $cols1 = array('att_status', 'SUM(att_status * attendance_master.no_of_lecture) as lectures_attended', 'SUM(attendance_master.no_of_lecture) as total_lectures', 'ROUND(SUM(att_status * attendance_master.no_of_lecture)*100/SUM(attendance_master.no_of_lecture)) as perecentage_att');
                    $last_present = $this->attendance_model->get_student_last_att($cols1, $where1, $conn);

                    if(count($last_present->row()) > 0){
                        $student->last_present = $last_present->row()->att_status ? $last_present->row()->att_status : '0'; 
                        $student->lectures_attended = $last_present->row()->lectures_attended ? $last_present->row()->lectures_attended : '0'; 
                        $student->total_lectures = $last_present->row()->total_lectures ? $last_present->row()->total_lectures : '0'; 
                        $student->perecentage_att = $last_present->row()->perecentage_att ? $last_present->row()->perecentage_att : '0'; 
                    }else{
                        $student->last_present = '0';
                        $student->lectures_attended = '0';
                        $student->total_lectures = '0';
                        $student->perecentage_att = '0';
                    }

                    if(!$student->profile){
                       $student->profile = '';
                    }

                    if(!isset($dataPost['subject_id']) || $dataPost['subject_id'] == 0){
                        $where_event = ['student_id'=>$student->student_id, 'event_attendance_list.class_id' => $dataPost['class_id']];

                        if($from_date != '' && $to_date != ''){
                            $where_event = ['student_id'=>$student->student_id, 'event_attendance_list.class_id' => $dataPost['class_id'], 'event_master.event_date >= ' => $from_date,'event_master.event_date <= ' => $to_date];
                        }

                        $cols_event =  ['ROUND(SUM(event_attendance_list.attendance)) as event_attendance'];

                        $result = $this->attendance_model->get_cols_events_join_where($cols_event, $where_event, $conn);

                        if(count($result->row()) > 0){
                            $event_attendance = $result->row()->event_attendance;
                            $student->lectures_attended = (string)((int)$student->lectures_attended + (int)$event_attendance); 
                            $total_lectures = (int)$student->total_lectures; 

                            if($total_lectures > 0){
                                $student->perecentage_att = round(100 * (int)$student->lectures_attended / $total_lectures);
                                if($student->perecentage_att > 100){
                                    $student->perecentage_att = "100";
                                }else{
                                    $student->perecentage_att = (string)$student->perecentage_att;
                                }
                            }else{
                                $student->perecentage_att = "100";
                            }
                            
                        }
                    }
                    
                    

                    if($student->profile!=''){
                       $student->profile = $root_uri."api/profile/".$student->profile;
                    } 
                }

                $response = [
                    'student_list' => $studentList
                ];

                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }


    private function getCollegeIDFromCode($college_code){
        $where = array('college_code'=>$college_code);
        $result = $this->attendance_model->get_multiple_where('college_master', $where, null);
        $college_id = $result->row()->college_id;
        return $college_id;
         
    }

   




}
