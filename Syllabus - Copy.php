<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Syllabus extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
       $this->load->model('syllabus_model');
    }


    public function subject_list_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();



        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->syllabus_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        $config = General::get_db_config($dataPost['college_code']);
        $conn = $this->syllabus_model->conn_db($config);

        if(!isset($dataPost['class_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Class ID is Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('class_id' => $dataPost['class_id']);
            $result = $this->syllabus_model->get_multiple_where('class_master', $where, $conn);

            if ( count($result->row()) <= 0 ) 
            {
                $response = [
                    'status' => REST_Controller::HTTP_UNAUTHORIZED,
                    'message' => 'Invalid Class ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {
                $config = General::get_db_config($dataPost['college_code']);
                $conn = $this->syllabus_model->conn_db($config);


                $where = array('class_id' => $dataPost['class_id'] );
                
                $subject_list = $this->syllabus_model->get_subject_list_where($where, $conn)->result();
                $response = [
                    'subject_list' => $subject_list
                ];
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }


    public function professor_subject_list_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();



        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->syllabus_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        $config = General::get_db_config($dataPost['college_code']);
        $conn = $this->syllabus_model->conn_db($config);

        if(!isset($dataPost['class_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Class ID is Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('class_id' => $dataPost['class_id']);
            $result = $this->syllabus_model->get_multiple_where('class_master', $where, $conn);

            if ( count($result->row()) <= 0 ) 
            {
                $response = [
                    'status' => REST_Controller::HTTP_UNAUTHORIZED,
                    'message' => 'Invalid Class ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {
                $config = General::get_db_config($dataPost['college_code']);
                $conn = $this->syllabus_model->conn_db($config);

                $where = array('email' => $token->id);
                $professor_id = $this->syllabus_model->get_cols_multiple_where(array('professor_id'), 'professor_master', $where, $conn)->row()->professor_id;

                $where = array('class_master.class_id' => $dataPost['class_id'],'professor_id' => $professor_id);
                
                $subject_list = $this->syllabus_model->get_professor_subject_list_where($where, $conn)->result();
                $response = [
                    'subject_list' => $subject_list
                ];
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function topic_list_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();



        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->syllabus_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        $config = General::get_db_config($dataPost['college_code']);
        $conn = $this->syllabus_model->conn_db($config);


        if(!isset($dataPost['subject_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Subject ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            $where = array('subject_id' => $dataPost['subject_id']);
            $config = General::get_db_config($dataPost['college_code']);
            $conn = $this->syllabus_model->conn_db($config);
            $result = $this->syllabus_model->get_multiple_where('subjects_offered', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Subject ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        };



       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                $where = array('email' => $token->id);
                $professor_id = $this->syllabus_model->get_cols_multiple_where(array('professor_id'), 'professor_master', $where, $conn)->row()->professor_id;


                $where = array('professor_id' => $professor_id, 'subject_id' => $dataPost['subject_id']);

                $is_prof_valid = $this->syllabus_model->get_cols_multiple_where(array('professor_id'), 'cs_professor_mapping', $where, $conn);

                if(count($is_prof_valid->row())<=0){
                    $response = [
                    'status' => REST_Controller::HTTP_UNAUTHORIZED,
                    'message' => 'Invalid Subject ID',
                    ];
                    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                    return;

                }



                $where = array('subject_id' => $dataPost['subject_id']);
                
                $topic_list = $this->syllabus_model->get_topic_list_where($where, $conn)->result();


                $response = [
                    'topic_list' => $topic_list
                ];
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function submit_topic_covered_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();



        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->syllabus_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        $config = General::get_db_config($dataPost['college_code']);
        $conn = $this->syllabus_model->conn_db($config);


        if(!isset($dataPost['att_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Attendance ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            $where = array('id' => $dataPost['att_id']);
            $config = General::get_db_config($dataPost['college_code']);
            $conn = $this->syllabus_model->conn_db($config);
            $result = $this->syllabus_model->get_multiple_where('attendance_master', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Attendance ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        };


        if(!isset($dataPost['topic_list'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Topics Covered List Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            $topics_covered_list = json_decode($dataPost['topic_list']);
            if(!$topics_covered_list){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Topics Covered List',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        }




       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {


                $topic_list = $topics_covered_list->topic_list;

                foreach ($topic_list as $topic) {
                    $data = array();
                    $data['topic_id'] = $topic->topic_id;
                    $data['status'] = $topic->status;
                    $data['att_id'] = $dataPost['att_id'];

                    $where = array('topic_id' => $topic->topic_id,'att_id' => $dataPost['att_id']);
                    $result = $this->syllabus_model->get_multiple_where('topics_covered', $where, $conn);

                    if ( count($result->row()) > 0 ) 
                    {
                        $this->syllabus_model->update_where('topics_covered', $where, $data,$conn);
                    } else {
                        $this->syllabus_model->insert('topics_covered',$data,$conn);
                    }
                }

                $response = [
                    'status' => REST_Controller::HTTP_OK,
                    'message' => 'Topics Covered Updated Successfully'
                ];
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;

            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function lecture_report_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();



        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->syllabus_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        $config = General::get_db_config($dataPost['college_code']);
        $conn = $this->syllabus_model->conn_db($config);


        if(!isset($dataPost['att_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Attendance ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            $where = array('id' => $dataPost['att_id']);
            $attendance_obj = $this->syllabus_model->get_multiple_where('attendance_master', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Attendance ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        };


       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {



            
                $cols = array('CONCAT(class_master.year_name,courses_offered.course_code," - ", class_master.class_division) as course_name', 'attendance_master.lecture_date', 'attendance_master.no_of_lecture', 'attendance_master.from_time', 'attendance_master.to_time', 'attendance_master.modified_on');
                $where = array('attendance_master.id' => $dataPost['att_id']);
                $lecture_report = $this->syllabus_model->get_lecture_report_where($cols, $where, $conn)->row();
                
                if(count($lecture_report)>0){
                    $lecture_report->from_time = date("h:i A", strtotime($lecture_report->from_time));
                    $lecture_report->to_time = date("h:i A", strtotime($lecture_report->to_time));
                    $lecture_report->modified_on = date("h:i A", strtotime($lecture_report->modified_on));
                }
                

                $cols = array('unit_id', 'unit_name');
                $where = array('subject_id' => $this->syllabus_model->get_multiple_where('attendance_master', array('id' => $dataPost['att_id']), $conn)->row()->subject_id);
                $result = $this->syllabus_model->get_cols_multiple_where($cols, 'unit_master', $where, $conn)->result();

                $subject = $this->syllabus_model->get_multiple_where('subjects_offered', $where, $conn)->row();
                  
                $unit_list = array();
                foreach ($result as $unit) {
                    $cols = array('topic_master.topic_id', 'topic_master.topic_name', 'topics_covered.status');
                    $where = array('attendance_master.id' => $dataPost['att_id'], 'unit_master.unit_id'=> $unit->unit_id);
                    $result = $this->syllabus_model->get_lecture_report_where($cols, $where, $conn)->result();
                  
                    if(count($result)){
                        array_push($unit_list, $unit);
                        $unit->topic_list = $result;
                    }
                }

                $where = array('attendance_master.id' => $dataPost['att_id']);
                $lecture_report->subject_code = $subject->subject_code;
                $lecture_report->subject_name = $subject->subject_name;
                
                $lecture_report->unit_list = $unit_list;
                $cols = array('SUM(attendance_list.att_status) as total_student_attendance');
                $total_attendance = $this->syllabus_model->get_lecture_attendance_where($cols, $where, $conn);
                $lecture_report->total_student_attendance = $total_attendance->row()->total_student_attendance;



                
               

                $response = [
                    'lecture_report' => $lecture_report
                ];
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;

            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function professor_syllabus_subject_status_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();



        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->syllabus_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };



        $config = General::get_db_config($dataPost['college_code']);
        $conn = $this->syllabus_model->conn_db($config);


       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                $where = array('email' => $token->id);

                $professor_id = $this->syllabus_model->get_cols_multiple_where(array('professor_id'), 'professor_master', $where, $conn)->row()->professor_id;

                $where = array('professor_id' => $professor_id);
                
                $subject_list = $this->syllabus_model->get_multiple_where('cs_professor_mapping', $where, $conn)->result();

                $syllabus_subject_list = array();

                foreach ($subject_list as $subject) {
                    $subject_status = array();
                    $subject_status['subject_id'] = $subject->subject_id;
                    $where = array('subject_id' => $subject->subject_id);
                    $result = $this->syllabus_model->get_multiple_where('subjects_offered', $where, $conn)->row();
                    $subject_status['subject_code'] = $result->subject_code;
                    $subject_status['subject_name'] = $result->subject_name;
                    $subject_status['class_id'] = $subject->class_id;

                    $cols = array('CONCAT(class_master.year_name,courses_offered.course_code) as course_name', 'class_master.class_division as class_division');
                    $where = array('class_master.class_id'=>$subject->class_id);
                    $class_list = $this->syllabus_model->get_college_class_list($cols, $where, $conn)->row();

                    $subject_status['course_name'] = $class_list->course_name;
                    $subject_status['class_division'] = $class_list->class_division;
                    
                    $cols = array('SUM(no_of_lecture) as total_lectures');
                    $where = array('professor_id' => $professor_id, 'subject_id' => $subject->subject_id, 'class_id' => $subject->class_id);
                    $result = $this->syllabus_model->get_cols_multiple_where( $cols, 'attendance_master', $where, $conn);


                    if(isset($result->row()->total_lectures)){
                        $subject_status['total_lectures'] = $result->row()->total_lectures;
                    }else{
                        $subject_status['total_lectures'] = '0';
                    }

                    $cols = array('COUNT(topic_master.topic_id) as total_syllabus');
                    $where = array('unit_master.mandatory' => 1, 'subject_id' => $subject_status['subject_id']);

                    $result = $this->syllabus_model->get_total_syllabus_where($cols, $where, $conn)->row();

                    $total_syllabus = $result->total_syllabus;

                    $per = 0;

                    $where = array('unit_master.mandatory' => 1, 'subject_id' => $subject->subject_id);
                    $result = $this->syllabus_model->get_multiple_where('unit_master', $where, $conn)->result();

                    $unit_per = 100 / count($result);

                    $syllabus_completed = 0;

                    foreach ($result as $unit) {

                        $where = array('unit_id' => $unit->unit_id);
                        $result = $this->syllabus_model->get_multiple_where('topic_master', $where, $conn)->result();
                        $topic_per = $unit_per / count($result);

                        foreach ($result as $topic) {
                            $cols = array('MAX( topics_covered.status ) as completed_syllabus');
                            $where = array('topics_covered.topic_id' => $topic->topic_id, 'attendance_master.class_id' => $subject->class_id);
                            //$where = array('attendance_master.subject_id' => $subject->subject_id, 'attendance_master.class_id' => $subject->class_id, 'topics_covered.topic_id' => $topic->topic_id);
                            $result = $this->syllabus_model->get_topic_status_where($cols, $where, $conn)->row();
                            $syllabus_completed+=((int)$result->completed_syllabus/2 * $topic_per);
                        }
                    }

                    if($total_syllabus > 0){
                        $per = $syllabus_completed;
                    }
                    $subject_status['syllabus_percentage'] = (string)round($per);
                    array_push($syllabus_subject_list, $subject_status);
                }

               

                $response = [
                    'syllabus_subject_list' => $syllabus_subject_list
                ];
                
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function student_syllabus_subject_status_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();



        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->syllabus_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        $config = General::get_db_config($dataPost['college_code']);
        $conn = $this->syllabus_model->conn_db($config);


       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                $where = array('email' => $token->id);

                $class_id = $this->syllabus_model->get_cols_multiple_where(array('class_id'), 'student_master', $where, $conn)->row()->class_id;

                $where = array('class_id' => $class_id);

                $course_id = $this->syllabus_model->get_cols_multiple_where(array('course_id'), 'class_master', $where, $conn)->row()->course_id;

                $where = array('course_id' => $course_id);
                
                $subject_list = $this->syllabus_model->get_multiple_where('subjects_offered', $where, $conn)->result();

                $syllabus_subject_list = array();

                foreach ($subject_list as $subject) {
                    $subject_status = array();
                    $subject_status['subject_id'] = $subject->subject_id;
                    $where = array('subject_id' => $subject->subject_id);
                    $result = $this->syllabus_model->get_multiple_where('subjects_offered', $where, $conn)->row();
                    $subject_status['subject_code'] = $result->subject_code;
                    $subject_status['subject_name'] = $result->subject_name;

                    $cols = array('SUM(no_of_lecture) as total_lectures');
                    $where = array('class_id' => $class_id, 'subject_id' => $subject->subject_id);
                    $result = $this->syllabus_model->get_cols_multiple_where( $cols, 'attendance_master', $where, $conn)->row();



                    if(isset($result->total_lectures)){
                        $subject_status['total_lectures'] = $result->total_lectures;
                    }else{
                        $subject_status['total_lectures'] = '0';
                    }

                    $cols = array('COUNT(topic_master.topic_id) as total_syllabus');
                    $where = array('unit_master.mandatory' => 1, 'subject_id' => $subject->subject_id);

                    $result = $this->syllabus_model->get_total_syllabus_where($cols, $where, $conn)->row();

                    $total_syllabus = $result->total_syllabus;

                    $per = 0;

                    $where = array('unit_master.mandatory' => 1, 'subject_id' => $subject->subject_id);
                    $result = $this->syllabus_model->get_multiple_where('unit_master', $where, $conn)->result();

                    $unit_per = 100 / count($result);

                    $syllabus_completed = 0;

                    foreach ($result as $unit) {

                        $where = array('unit_id' => $unit->unit_id);
                        $result = $this->syllabus_model->get_multiple_where('topic_master', $where, $conn)->result();
                        $topic_per = $unit_per / count($result);
                        
                        foreach ($result as $topic) {
                            $cols = array('MAX( topics_covered.status ) as completed_syllabus');
                            $where = array('topics_covered.topic_id' => $topic->topic_id, 'attendance_master.class_id' => $class_id);
                            //$where = array('attendance_master.subject_id' => $subject->subject_id, 'attendance_master.class_id' => $class_id, 'topics_covered.topic_id' => $topic->topic_id);
                            $result = $this->syllabus_model->get_topic_status_where($cols, $where, $conn)->row();
                            $syllabus_completed+=((int)$result->completed_syllabus/2 * $topic_per);
                        }
                    }

                    if($total_syllabus > 0){
                        $per = $syllabus_completed;
                    }

                    $subject_status['syllabus_percentage'] = (string)round($per);
                    array_push($syllabus_subject_list, $subject_status);
                }

               

                $response = [
                    'syllabus_subject_list' => $syllabus_subject_list
                ];
                
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function college_syllabus_subject_status_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();



        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->syllabus_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        $config = General::get_db_config($dataPost['college_code']);
        $conn = $this->syllabus_model->conn_db($config);

        if(!isset($dataPost['class_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Class ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            $where = array('class_id' => $dataPost['class_id']);
            $result = $this->syllabus_model->get_multiple_where('class_master', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Class ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        };


       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                

                $where = array('class_id' => $dataPost['class_id']);

                $course_id = $this->syllabus_model->get_cols_multiple_where(array('course_id'), 'class_master', $where, $conn)->row()->course_id;

                $where = array('course_id' => $course_id);
                
                $subject_list = $this->syllabus_model->get_multiple_where('subjects_offered', $where, $conn)->result();

                $syllabus_completed = 0;

                $syllabus_subject_list = array();

                foreach ($subject_list as $subject) {
                    $subject_status = array();
                    $subject_status['subject_id'] = $subject->subject_id;
                    $where = array('subject_id' => $subject->subject_id);
                    $result = $this->syllabus_model->get_multiple_where('subjects_offered', $where, $conn)->row();
                    $subject_status['subject_code'] = $result->subject_code;
                    $subject_status['subject_name'] = $result->subject_name;

                    $cols = array('SUM(no_of_lecture) as total_lectures');
                    $where = array('class_id' => $dataPost['class_id'], 'subject_id' => $subject->subject_id);
                    $result = $this->syllabus_model->get_cols_multiple_where( $cols, 'attendance_master', $where, $conn)->row();



                    if(isset($result->total_lectures)){
                        $subject_status['total_lectures'] = $result->total_lectures;
                    }else{
                        $subject_status['total_lectures'] = '0';
                    }

                    $cols = array('COUNT(topic_master.topic_id) as total_syllabus');
                    $where = array('unit_master.mandatory' => 1, 'subject_id' => $subject->subject_id);

                    $result = $this->syllabus_model->get_total_syllabus_where($cols, $where, $conn)->row();

                    $total_syllabus = $result->total_syllabus;

                    $per = 0;

                    $where = array('unit_master.mandatory' => 1, 'subject_id' => $subject->subject_id);
                    $result = $this->syllabus_model->get_multiple_where('unit_master', $where, $conn)->result();

                    $unit_per = 100 / count($result);

                    $syllabus_completed = 0;

                    foreach ($result as $unit) {

                        $where = array('unit_id' => $unit->unit_id);
                        $result = $this->syllabus_model->get_multiple_where('topic_master', $where, $conn)->result();
                        $topic_per = $unit_per / count($result);

                        

                        foreach ($result as $topic) {
                            $cols = array('MAX( topics_covered.status ) as completed_syllabus');
                            $where = array('topics_covered.topic_id' => $topic->topic_id, 'attendance_master.class_id' => $dataPost['class_id']);
                            //$where = array('attendance_master.subject_id' => $subject->subject_id, 'attendance_master.class_id' => $dataPost['class_id'], 'topics_covered.topic_id' => $topic->topic_id);
                            $result = $this->syllabus_model->get_topic_status_where($cols, $where, $conn)->row();
                            $syllabus_completed+=((int)$result->completed_syllabus/2 * $topic_per);
                        }

                        

                        
                    }

                    if($total_syllabus > 0){
                        $per = $syllabus_completed;
                    }

                    

                    /*
                    --- OLD ---
                    if($total_syllabus > 0){

                        $cols = array('MAX( topics_covered.status ) as completed_syllabus');
                        $where = array('unit_master.mandatory' => 1, 'attendance_master.subject_id' => $subject->subject_id, 'attendance_master.class_id' => $dataPost['class_id']);
                        $result = $this->syllabus_model->get_syllabus_status_where($cols, $where, $conn)->result();

                        $syllabus_completed = 0;

                        foreach ($result as $value) {
                            $syllabus_completed+=(int)$value->completed_syllabus;
                        }

                        $syllabus_completed = $syllabus_completed / 2; //as 2 is the completed status

                        $per = $syllabus_completed * 100/$total_syllabus;
                    }
                    --- OLD ---
                    */

                    $subject_status['syllabus_percentage'] = (string)round($per);
                    array_push($syllabus_subject_list, $subject_status);
                }

               

                $response = [
                    'syllabus_subject_list' => $syllabus_subject_list
                ];
                
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function professor_detailed_syllabus_status_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();



        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->syllabus_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        $config = General::get_db_config($dataPost['college_code']);
        $conn = $this->syllabus_model->conn_db($config);

        if(!isset($dataPost['subject_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Subject ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            $where = array('subject_id' => $dataPost['subject_id']);
            $result = $this->syllabus_model->get_multiple_where('subjects_offered', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Subject ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        };

        if(!isset($dataPost['class_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Class ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            $where = array('class_id' => $dataPost['class_id']);
            $result = $this->syllabus_model->get_multiple_where('class_master', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Class ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        };


       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                $where = array('email' => $token->id);
                $professor_id = $this->syllabus_model->get_cols_multiple_where(array('professor_id'), 'professor_master', $where, $conn)->row()->professor_id;


                $where = array('professor_id' => $professor_id, 'subject_id' => $dataPost['subject_id']);

                $is_prof_valid = $this->syllabus_model->get_cols_multiple_where(array('professor_id'), 'cs_professor_mapping', $where, $conn);

                if(count($is_prof_valid->row())<=0){
                    $response = [
                    'status' => REST_Controller::HTTP_UNAUTHORIZED,
                    'message' => 'Invalid Subject ID',
                    ];
                    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                    return;

                }

                $cols = array('COUNT(topic_master.topic_id) as total_syllabus');
                $where = array('unit_master.mandatory' => 1, 'subject_id' => $dataPost['subject_id']);

                $result = $this->syllabus_model->get_total_syllabus_where($cols, $where, $conn)->row();

                $total_syllabus = $result->total_syllabus;

                $per = 0;

                $where = array('unit_master.mandatory' => 1, 'subject_id' => $dataPost['subject_id']);
                $result = $this->syllabus_model->get_multiple_where('unit_master', $where, $conn)->result();

                $unit_per = 100 / count($result);

                //print_r($unit_per);

                $syllabus_completed = 0;

                foreach ($result as $unit) {

                    $where = array('unit_id' => $unit->unit_id);
                    $result = $this->syllabus_model->get_multiple_where('topic_master', $where, $conn)->result();
                    
                    $topic_per = $unit_per / count($result);
                   
                    foreach ($result as $topic) {
                        $cols = array('MAX( topics_covered.status ) as completed_syllabus');
                        $where = array('topics_covered.topic_id' => $topic->topic_id, 'attendance_master.class_id' => $dataPost['class_id']);
                        $result = $this->syllabus_model->get_topic_status_where($cols, $where, $conn)->row();
                        
                        $syllabus_completed+=((int)$result->completed_syllabus/2 * $topic_per);
                        
                    }
                }

                if($total_syllabus > 0){
                    $per = $syllabus_completed;
                }
                $per = (string)round($per);

                $unit_syllabus_array = array();


                $where = array('subject_id' => $dataPost['subject_id'], 'mandatory' => 1);
                
                $unit_list = $this->syllabus_model->get_multiple_where('unit_master', $where, $conn)->result();

                foreach ($unit_list as $unit) {
                    $unit_object = array();
                    $unit_object['unit_name'] = $unit->unit_name;
                    $unit_object['unit_id'] = $unit->unit_id;

                    $where = array('unit_id' => $unit->unit_id);
                    $result = $this->syllabus_model->get_multiple_where('topic_master', $where, $conn)->result();
                    $topic_list = array();
                    foreach ($result as $topic) {
                        $topic_object = array();
                        $topic_object['topic_name'] = $topic->topic_name;
                        $topic_object['topic_id'] = $topic->topic_id;
                        $topic_object['topic_description'] = $topic->topic_description;

                        $cols = array('MAX( topics_covered.status ) as completed_syllabus');
                        $where = array('topics_covered.topic_id' => $topic->topic_id, 'attendance_master.class_id' => $dataPost['class_id']);
                        $result = $this->syllabus_model->get_topic_status_where($cols, $where, $conn);

                        if(count($result->row())>0){
                            $topic_object['status'] = ($result->row()->completed_syllabus)?$result->row()->completed_syllabus:'0';
                        }else{
                            $topic_object['status'] = 0;
                        }         
                        

                        array_push($topic_list, $topic_object);
                    }

                    $unit_object['topic_list'] = $topic_list;



                    array_push($unit_syllabus_array, $unit_object);
                }
                
               

                $response = [
                    'syllabus_percentage' => $per,
                    'unit_syllabus_array' => $unit_syllabus_array
                ];
                
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function student_detailed_syllabus_status_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();



        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->syllabus_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        $config = General::get_db_config($dataPost['college_code']);
        $conn = $this->syllabus_model->conn_db($config);

        if(!isset($dataPost['subject_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Subject ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            $where = array('subject_id' => $dataPost['subject_id']);
            $result = $this->syllabus_model->get_multiple_where('subjects_offered', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Subject ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        };


       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                $where = array('email' => $token->id);
                $class_id = $this->syllabus_model->get_cols_multiple_where(array('class_id'), 'student_master', $where, $conn)->row()->class_id;

                $where = array('class_id' => $class_id);

                $course_id = $this->syllabus_model->get_cols_multiple_where(array('course_id'), 'class_master', $where, $conn)->row()->course_id;

                $where = array('course_id' => $course_id, 'subject_id' => $dataPost['subject_id']);
                
                $is_prof_valid = $this->syllabus_model->get_multiple_where('subjects_offered', $where, $conn);

                if(count($is_prof_valid->row())<=0){
                    $response = [
                    'status' => REST_Controller::HTTP_UNAUTHORIZED,
                    'message' => 'Invalid Subject ID',
                    ];
                    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                    return;

                }


                $cols = array('COUNT(topic_master.topic_id) as total_syllabus');
                $where = array('unit_master.mandatory' => 1, 'subject_id' => $dataPost['subject_id']);

                $result = $this->syllabus_model->get_total_syllabus_where($cols, $where, $conn)->row();

                $total_syllabus = $result->total_syllabus;

                $per = 0;

                $where = array('unit_master.mandatory' => 1, 'subject_id' => $dataPost['subject_id']);
                $result = $this->syllabus_model->get_multiple_where('unit_master', $where, $conn)->result();

                $unit_per = 100 / count($result);

                $syllabus_completed = 0;

                foreach ($result as $unit) {

                    $where = array('unit_id' => $unit->unit_id);
                    $result = $this->syllabus_model->get_multiple_where('topic_master', $where, $conn)->result();
                    $topic_per = $unit_per / count($result);
                    
                    foreach ($result as $topic) {
                        $cols = array('MAX( topics_covered.status ) as completed_syllabus');
                        $where = array('topics_covered.topic_id' => $topic->topic_id, 'attendance_master.class_id' => $class_id);
                        //$where = array('attendance_master.subject_id' => $dataPost['subject_id'], 'attendance_master.class_id' => $class_id, 'topics_covered.topic_id' => $topic->topic_id);
                        $result = $this->syllabus_model->get_topic_status_where($cols, $where, $conn)->row();
                        $syllabus_completed+=((int)$result->completed_syllabus/2 * $topic_per);
                    }
                }

                if($total_syllabus > 0){
                    $per = $syllabus_completed;
                }
                $per = (string)round($per);

                $unit_syllabus_array = array();


                $where = array('subject_id' => $dataPost['subject_id'], 'mandatory' => 1);
                
                $unit_list = $this->syllabus_model->get_multiple_where('unit_master', $where, $conn)->result();

                foreach ($unit_list as $unit) {
                    $unit_object = array();
                    $unit_object['unit_name'] = $unit->unit_name;

                    $where = array('unit_id' => $unit->unit_id);
                    $result = $this->syllabus_model->get_multiple_where('topic_master', $where, $conn)->result();
                    $topic_list = array();
                    foreach ($result as $topic) {
                        $topic_object = array();
                        $topic_object['topic_name'] = $topic->topic_name;
                        $topic_object['topic_description'] = $topic->topic_description;

                        $cols = array('MAX( topics_covered.status ) as completed_syllabus');
                        $where = array('topics_covered.topic_id' => $topic->topic_id, 'attendance_master.class_id' => $class_id);
                        $result = $this->syllabus_model->get_topic_status_where($cols, $where, $conn);

                        if(count($result->row())>0){
                            $topic_object['status'] = ($result->row()->completed_syllabus)?$result->row()->completed_syllabus:'0';
                        }else{
                            $topic_object['status'] = "0";
                        }           
                        

                        array_push($topic_list, $topic_object);
                    }

                    $unit_object['topic_list'] = $topic_list;



                    array_push($unit_syllabus_array, $unit_object);
                }
                
               

                $response = [
                    'syllabus_percentage' => $per,
                    'unit_syllabus_array' => $unit_syllabus_array
                ];
                
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function college_detailed_syllabus_status_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();



        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->syllabus_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        $config = General::get_db_config($dataPost['college_code']);
        $conn = $this->syllabus_model->conn_db($config);

        if(!isset($dataPost['subject_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Subject ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            $where = array('subject_id' => $dataPost['subject_id']);
            $result = $this->syllabus_model->get_multiple_where('subjects_offered', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Subject ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        };

        if(!isset($dataPost['class_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Class ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            $where = array('class_id' => $dataPost['class_id']);
            $result = $this->syllabus_model->get_multiple_where('class_master', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Class ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        };


       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                
                $where = array('class_id' => $dataPost['class_id']);

                $course_id = $this->syllabus_model->get_cols_multiple_where(array('course_id'), 'class_master', $where, $conn)->row()->course_id;

                $where = array('course_id' => $course_id, 'subject_id' => $dataPost['subject_id']);
                
                $is_prof_valid = $this->syllabus_model->get_multiple_where('subjects_offered', $where, $conn);

                if(count($is_prof_valid->row())<=0){
                    $response = [
                    'status' => REST_Controller::HTTP_UNAUTHORIZED,
                    'message' => 'Invalid Subject ID',
                    ];
                    $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                    return;

                }


                $cols = array('COUNT(topic_master.topic_id) as total_syllabus');
                $where = array('unit_master.mandatory' => 1, 'subject_id' => $dataPost['subject_id']);

                $result = $this->syllabus_model->get_total_syllabus_where($cols, $where, $conn)->row();

                $total_syllabus = $result->total_syllabus;

                $per = 0;

                $where = array('unit_master.mandatory' => 1, 'subject_id' => $dataPost['subject_id']);
                $result = $this->syllabus_model->get_multiple_where('unit_master', $where, $conn)->result();

                $unit_per = 100 / count($result);

                $syllabus_completed = 0;

                foreach ($result as $unit) {

                    $where = array('unit_id' => $unit->unit_id);
                    $result = $this->syllabus_model->get_multiple_where('topic_master', $where, $conn)->result();
                    $topic_per = $unit_per / count($result);

                    

                    foreach ($result as $topic) {
                        $cols = array('MAX( topics_covered.status ) as completed_syllabus');
                        $where = array('topics_covered.topic_id' => $topic->topic_id, 'attendance_master.class_id' => $dataPost['class_id']);
                        //$where = array('attendance_master.subject_id' => $dataPost['subject_id'], 'attendance_master.class_id' => $dataPost['class_id'], 'topics_covered.topic_id' => $topic->topic_id);
                        $result = $this->syllabus_model->get_topic_status_where($cols, $where, $conn)->row();
                        $syllabus_completed+=((int)$result->completed_syllabus/2 * $topic_per);
                    }

                    

                    
                }

                if($total_syllabus > 0){
                    $per = $syllabus_completed;
                }

                $unit_syllabus_array = array();


                $where = array('subject_id' => $dataPost['subject_id'], 'mandatory' => 1);
                
                $unit_list = $this->syllabus_model->get_multiple_where('unit_master', $where, $conn)->result();

                foreach ($unit_list as $unit) {
                    $unit_object = array();
                    $unit_object['unit_name'] = $unit->unit_name;

                    $where = array('unit_id' => $unit->unit_id);
                    $result = $this->syllabus_model->get_multiple_where('topic_master', $where, $conn)->result();
                    $topic_list = array();
                    foreach ($result as $topic) {
                        $topic_object = array();
                        $topic_object['topic_name'] = $topic->topic_name;
                        $topic_object['topic_description'] = $topic->topic_description;

                        $cols = array('MAX( topics_covered.status ) as completed_syllabus');
                        $where = array('topics_covered.topic_id' => $topic->topic_id, 'attendance_master.class_id' => $dataPost['class_id']);
                        $result = $this->syllabus_model->get_topic_status_where($cols, $where, $conn);

                        if(count($result->row())>0){
                            $topic_object['status'] = ($result->row()->completed_syllabus)?$result->row()->completed_syllabus:'0';
                        }else{
                            $topic_object['status'] = "0";
                        }         
                        

                        array_push($topic_list, $topic_object);
                    }

                    $unit_object['topic_list'] = $topic_list;



                    array_push($unit_syllabus_array, $unit_object);
                }
                
               

                $response = [
                    'syllabus_percentage' => (string)round($per),
                    'unit_syllabus_array' => $unit_syllabus_array
                ];
                
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function college_class_syllabus_status_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();



        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->syllabus_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        $config = General::get_db_config($dataPost['college_code']);
        $conn = $this->syllabus_model->conn_db($config);

        


       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                
                
                $cols = array('CONCAT(class_master.year_name,courses_offered.course_code," - ", class_master.class_division) as course_name', 'class_master.class_id');
                $where = array('*');
                $class_list = $this->syllabus_model->get_college_class_list($cols, $where, $conn)->result();


                foreach ($class_list as $class) {

                    $where = array('class_id' => $class->class_id);
                    $subject_list = $this->syllabus_model->get_subject_list_where($where, $conn)->result();
                    $total_subjects = 0;
                    $total_percent = 0;

                    foreach ($subject_list as $subject) {
                        $cols = array('COUNT(topic_master.topic_id) as total_syllabus');
                        $where = array('unit_master.mandatory' => 1, 'subject_id' => $subject->subject_id);

                        $result = $this->syllabus_model->get_total_syllabus_where($cols, $where, $conn)->row();

                        $total_syllabus = $result->total_syllabus;

                        $per = 0;

                        $where = array('unit_master.mandatory' => 1, 'subject_id' => $subject->subject_id);
                        $result = $this->syllabus_model->get_multiple_where('unit_master', $where, $conn)->result();

                        $unit_per = 100 / count($result);

                        $syllabus_completed = 0;

                        foreach ($result as $unit) {

                            $where = array('unit_id' => $unit->unit_id);
                            $result = $this->syllabus_model->get_multiple_where('topic_master', $where, $conn)->result();
                            $topic_per = $unit_per / count($result);

                            

                            foreach ($result as $topic) {
                                $cols = array('MAX( topics_covered.status ) as completed_syllabus');
                                $where = array('topics_covered.topic_id' => $topic->topic_id, 'attendance_master.class_id' => $dataPost['class_id']);
                                //$where = array('attendance_master.subject_id' => $subject->subject_id, 'attendance_master.class_id' => $class->class_id, 'topics_covered.topic_id' => $topic->topic_id);
                                $result = $this->syllabus_model->get_topic_status_where($cols, $where, $conn)->row();
                                $syllabus_completed+=((int)$result->completed_syllabus/2 * $topic_per);
                            }

                            

                            
                        }
                        //print_r($syllabus_completed);

                        if($total_syllabus > 0){
                            $per = $syllabus_completed;
                        }

                        $total_percent = $total_percent + round($per);
                        $total_subjects = $total_subjects + 1;
                    }


                    $class->status = (string)round($total_percent / $total_subjects);

                }


                $response = [
                    'class_list' => $class_list
                ];
                
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function professor_class_list_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();



        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->syllabus_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        $config = General::get_db_config($dataPost['college_code']);
        $conn = $this->syllabus_model->conn_db($config);



       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                $where = array('email' => $token->id);
                $professor_id = $this->syllabus_model->get_cols_multiple_where(array('professor_id'), 'professor_master', $where, $conn)->row()->professor_id;

                $cols = array('CONCAT(class_master.year_name,courses_offered.course_code," - ", class_master.class_division) as course_name', 'class_master.class_id');
                $where = array('cs_professor_mapping.professor_id' => $professor_id);
                $class_list = $this->syllabus_model->get_professor_class_list($cols, $where, $conn)->result();
                       

                $response = [
                    'class_list' => $class_list
                ];
                
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    public function detailed_log_subject_post(){
        $headers = $this->input->request_headers();
        $headers = Authorization::updateKey($headers);
        $dataPost = $this->input->post();



        if(!isset($dataPost['college_code'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'College Code Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('college_code' => $dataPost['college_code']);
            $result = $this->syllabus_model->get_multiple_where('college_master', $where, null);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid College Code',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };

        $config = General::get_db_config($dataPost['college_code']);
        $conn = $this->syllabus_model->conn_db($config);

        if(!isset($dataPost['subject_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Subject ID Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            $where = array('subject_id' => $dataPost['subject_id']);
            $config = General::get_db_config($dataPost['college_code']);
            $conn = $this->syllabus_model->conn_db($config);
            $result = $this->syllabus_model->get_multiple_where('subjects_offered', $where, $conn);

            if(count($result->row())<=0){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid Subject ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
        };


        if(!isset($dataPost['class_id'])){
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Class ID is Required',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }else{
            
            $where = array('class_id' => $dataPost['class_id']);
            $result = $this->syllabus_model->get_multiple_where('class_master', $where, $conn);

            if ( count($result->row()) <= 0 ) 
            {
                $response = [
                    'status' => REST_Controller::HTTP_UNAUTHORIZED,
                    'message' => 'Invalid Class ID',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }

        };


        $from_date = '';
        $to_date = '';

        if(isset($dataPost['from_date'])){
            if(!General::getDateValidation($dataPost['from_date'])){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid From Date',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
            $from_date = date($dataPost['from_date']);
        }

        if(isset($dataPost['to_date'])){
            if(!General::getDateValidation($dataPost['to_date'])){
                $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Invalid From Date',
                ];
                $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                return;
            }
            $to_date = date($dataPost['to_date']);
        }


        if($from_date != '' && $to_date != ''){
            $where = array('lecture_date >= ' => $from_date,'lecture_date <= ' => $to_date, 'subject_id' => $dataPost['subject_id']);
            $result = $this->syllabus_model->get_multiple_where('attendance_master', $where, $conn);
            if(count($result->row()) <= 0){
                 $response = [
                'status' => REST_Controller::HTTP_OK,
                'message' => 'No records found for this Date Range',
                ];
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }  
        }



       if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateToken($headers['Authorization']);
            if ($token != false) {

                $where = array('email' => $token->id);
                $professor_id = $this->syllabus_model->get_cols_multiple_where(array('professor_id'), 'professor_master', $where, $conn)->row()->professor_id;


                $where = array('attendance_master.professor_id' => $professor_id, 'attendance_master.subject_id' => $dataPost['subject_id'], 'attendance_master.class_id' => $dataPost['class_id']);

                if($from_date != '' && $to_date != ''){
                    $where = array('attendance_master.professor_id' => $professor_id, 'attendance_master.subject_id' => $dataPost['subject_id'],'attendance_master.lecture_date >= ' => $from_date,'attendance_master.lecture_date <= ' => $to_date, 'attendance_master.class_id' => $dataPost['class_id']);
                }

                $result = $this->syllabus_model->get_multiple_where('attendance_master', $where, $conn)->result();

                $log_array = array();

                foreach ($result as $attendance) {
                    $cols = array('CONCAT(class_master.year_name,courses_offered.course_code," - ", class_master.class_division) as course_name', 'attendance_master.lecture_date', 'attendance_master.no_of_lecture', 'attendance_master.from_time', 'attendance_master.to_time', 'attendance_master.modified_on as date_of_submission');
                    $where = array('attendance_master.id' => $attendance->id, 'attendance_master.class_id' => $dataPost['class_id']);
                    $lecture_report = $this->syllabus_model->get_lecture_report_where($cols, $where, $conn)->row();
                    
                    if(count($lecture_report)>0){
                        $lecture_report->from_time = date("h:i A", strtotime($lecture_report->from_time));
                        $lecture_report->to_time = date("h:i A", strtotime($lecture_report->to_time));

                        $lecture_report->date_of_submission = date("h:i A", strtotime($lecture_report->date_of_submission));
                    }
                    

                    $cols = array('unit_id', 'unit_name');
                    $where = array('subject_id' => $this->syllabus_model->get_multiple_where('attendance_master', array('id' => $attendance->id), $conn)->row()->subject_id);
                    $result = $this->syllabus_model->get_cols_multiple_where($cols, 'unit_master', $where, $conn)->result();
                    $unit_list = array();
                    foreach ($result as $unit) {
                        $cols = array('topic_master.topic_id', 'topic_master.topic_name', 'topics_covered.status');
                        $where = array('attendance_master.id' => $attendance->id, 'unit_master.unit_id'=> $unit->unit_id);
                        $result = $this->syllabus_model->get_lecture_report_where($cols, $where, $conn)->result();

                        if(count($result)){
                            array_push($unit_list, $unit);
                            $unit->topic_list = $result;
                        }
                    }

                    $where = array('attendance_master.id' => $attendance->id);



                    

                    if(empty((array)$lecture_report)){
                        $lecture_report = new stdClass();
                    }

                    $lecture_report->unit_list = $unit_list;
                    $cols = array('SUM(attendance_list.att_status) as total_student_attendance');
                    $total_attendance = $this->syllabus_model->get_lecture_attendance_where($cols, $where, $conn);
                    $lecture_report->total_student_attendance = $total_attendance->row()->total_student_attendance;

                    array_push($log_array, $lecture_report);
                }

                
                       

                $response = [
                    'subject_logs' => $log_array
                ];
                
                $this->set_response($response, REST_Controller::HTTP_OK);
                return;
            }
            $response = [
                'status' => REST_Controller::HTTP_UNAUTHORIZED,
                'message' => 'Unauthorized',
            ];
            $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
            return;
        }
        $response = [
            'status' => REST_Controller::HTTP_FORBIDDEN,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, REST_Controller::HTTP_FORBIDDEN);
    }

    
}
